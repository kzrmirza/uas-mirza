from django.urls import path
from . import views
app_name = "absen"

urlpatterns = [
    path('', views.index, name="index"),
]